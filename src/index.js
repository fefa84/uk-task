const fs = require("fs");
const path = require("path");
const { cursorTo } = require("readline");

(function init() {
  const users = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/users.json"), "utf-8"));
  const mobileDevices = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/mobile_devices.json"), "utf-8"));
  const iotDevices = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/iot_devices.json"), "utf-8"));
  console.log(new Date().toISOString());
  console.log(count(users, mobileDevices, iotDevices));
  console.log(new Date().toISOString());
})();



function count(users, mobileDevices, iotDevices) {
  // your code goes here

  const dpm = groupDevicesByMobile(iotDevices);
  

  const ubn = groupUsersByName(users);
 
  
  const mobilesPerName={};


  for (let key in ubn) {

    if (!(key in mobilesPerName)) {
          mobilesPerName[key] = [];
    } 
   
    for (let i=0; i<ubn[key].length; i++){
      for (let j=0; j<mobileDevices.length; j++) {

        if (ubn[key][i] === mobileDevices[j]['user']){
          mobilesPerName[key].push(mobileDevices[j]['id'])
        }
      }
    }
  }

  const devicesPerName = {};

  for (let key in mobilesPerName) {
      if (!(key in devicesPerName)) {
            devicesPerName[key] = 0;
        }

      for (let i=0; i<mobilesPerName[key].length; i++){
        
        devicesPerName[key]+=(dpm[mobilesPerName[key][i]]);
      }
    }
    console.log(devicesPerName);
    
  }


function groupDevicesByMobile(iotDevices){
  let devicesPerMobile = {};

  for (let i = 0; i < iotDevices.length; i++) {
      if (!(iotDevices[i]['mobile'] in devicesPerMobile)) {
          devicesPerMobile[iotDevices[i]['mobile']] = 0;
      }
      devicesPerMobile[iotDevices[i]['mobile']]++;
  }

  return devicesPerMobile;
}


function groupUsersByName(users) {

  const usersByName = {};

  const regex=/ *- *[\s\S]+/;

  for (let i = 0; i < users.length; i++) {
    let name = users[i]['name'].replace(regex, "");
    if (!(name in usersByName)) {
     
      usersByName[name] = [];
    }
    usersByName[name].push(users[i]['id']);
  }

  return usersByName;
  }





/*Part of second solution*/

// function count(users, mobileDevices, iotDevices) {

//   const dpm = groupDevicesByMobile(iotDevices);
//   console.log(dpm);

 
  // const regex=/ *- *[\s\S]+/;

// users.map((item)=> {
    
//     if (!('mobiles' in item)) {
//       item['mobiles'] = [];
    
//     }
//     if (!('totalNumOfDev' in item)) {
//       item['totalNumOfDev'] = 0;
//     }
//     for (let i =0; i<mobileDevices.length;i++){

//       if(item.id === mobileDevices[i].user) {
//         item['mobiles'].push(mobileDevices[i].id);
        
//           for (let key in dpm) {
            
//             if (key ===mobileDevices[i].id ){
//               item['totalNumOfDev']+=dpm[key];
//             }
//           }
//         }
//       }
      
//     });
//     console.log(users)

//   }

//   function groupDevicesByMobile(iotDevices){
//     let devicesPerMobile = {};
  
//     for (let i = 0; i < iotDevices.length; i++) {
//         if (!(iotDevices[i]['mobile'] in devicesPerMobile)) {
//             devicesPerMobile[iotDevices[i]['mobile']] = 0;
//         }
//         devicesPerMobile[iotDevices[i]['mobile']]++;
//     }
  
//     return devicesPerMobile;
//   }
  
